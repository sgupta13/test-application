﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace callbacklistener.Models
{
    //Notification model 
    public class ESLNotificationsModel
    {
        public string name { get; set; }
        public string sessionUser { get; set; }
        public string packageId { get; set; }
        public string message { get; set; }
        public string documentId { get; set; }
    }
}