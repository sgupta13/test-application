﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace callbacklistener.Models
{
    public enum NotificationEventsType
    {
        PACKAGE_ACTIVATE,
        PACKAGE_COMPLETE,
        PACKAGE_CREATE,
        PACKAGE_DEACTIVATE,
        PACKAGE_DECLINE,
        PACKAGE_DELETE,
        PACKAGE_READY_FOR_COMPLETE,
        PACKAGE_RESTORE,
        PACKAGE_TRASH,
        PACKAGE_EXPIRE,
        PACKAGE_OPT_OUT,
        SIGNER_COMPLETE,
        DOCUMENT_SIGNED,
        ROLE_REASSIGN,
        KBA_FAILURE,
        EMAIL_BOUNCE,
        PACKAGE_ATTACHMENT,
        SIGNER_LOCKED
    }
}