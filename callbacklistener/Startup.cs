﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(callbacklistener.Startup))]
namespace callbacklistener
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
