﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using callbacklistener.Models;
using System.Diagnostics;

namespace callbacklistener.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public void Index(ESLNotificationsModel notification)
        {
            Trace.WriteLine("Notification " + notification.name + " for package " + notification.packageId + " received");

            Trace.WriteLine("===================================Additional details===================================");

            if(String.IsNullOrEmpty(notification.message))
            Trace.WriteLine("Message: " + notification.message);

            if(String.IsNullOrEmpty(notification.sessionUser))
            Trace.WriteLine("User: " + notification.sessionUser);

            if(String.IsNullOrEmpty(notification.documentId))
            Trace.WriteLine("Document ID " + notification.documentId);
        }


    }
}